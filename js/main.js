function changeColor(item){
    if (item.style.color == "black" || item.style.color == "") {
        item.style.color = "red";
    }
    else {
        item.style.color = "black";
    }
    return false;
}
var flag=0;
$(".tuning").on('click', function () {
    if (flag%2 == 0){
        $(".user_icon").css('color','#fff')
    }
     else {
        $(".user_icon").css('color','#004eaf')
    }
    flag++;
});

let hiddenBox = $("div.up_arrow_container");
$( window ). scroll( function() {
    if ($( window ). scrollTop() > 10)
        hiddenBox.show();
    else hiddenBox.hide();
});

$("div.up_arrow_container").click(function() {
    $('body,html').animate({scrollTop:0},400);
});

$(".bg_lines").click(function() {
    $('.topmenu ul').slideToggle(300, function(){
        if($(this).css('display') === 'none'){
            $(this).removeAttr('style');
        }
    });
});
//set width mobile-photo-description container
$(document).ready(function () {
    var w = $(window).width(),
        pw = $(".photo-container").width(),
        aw = $(".audio-container").width(),
        vw = $(".video-container").width(),
        baci = $(".audio-container"),
        bpci = $(".photo-container"),
        hac = $(".audio_content audio").height();
    if (w<541) {
        $(".mobile-photo-description").css('width', pw+'px');
        $(".mobile-audio-description").css('width', aw+'px');
        $(".mobile-video-description").css('width', vw+'px');
        baci.each(function () {
            var row_bottom = $(this).parent().offset().top - $(this).parent().outerHeight();
            var this_bottom = $(this).find("img").offset().top - $(this).find("img").outerHeight();
            var abs_bottom = this_bottom - row_bottom - 25;
            $(this).parent().find(".mobile-audio-description").css('bottom', abs_bottom + 'px');
        });
        bpci.each(function () {
            var row_bottom = $(this).parent().offset().top - $(this).parent().outerHeight();
            var this_bottom = $(this).find("img").offset().top - $(this).find("img").outerHeight();
            var abs_bottom = this_bottom - row_bottom - 25;
            $(this).parent().find(".mobile-audio-description").css('bottom', abs_bottom + 'px');
        });

    }
    let height = document.documentElement.offsetHeight;
    let width = document.documentElement.offsetWidth;
    let cont_height = height - 234;
    let cont = $(".content");
    let h = parseInt(cont.css("height"));
    if ( h < cont_height && width > 1000) {
        cont.animate({height:cont_height},500,"linear");
    }

    if (height > 900) {
        let cont = $(".content");
        cont.animate({'min-height':'700'},700,"linear");
    }
});
