<?php
/**
 * Created by PhpStorm.
 * User: DM
 * Date: 12.06.2019
 * Time: 21:44
 */
require_once ('Entree.php');
require_once ('Ingredient.php');

class ExEntree extends Entree {
    public function __construct($name, $ingredients)
    {
        $this->ingredients = $ingredients;
        parent::__construct($name, $ingredients);
    }
    public function getTotalCost(){
        $total = 0;
        foreach ($this->ingredients as $ing) {
            $total += $ing->getPrice();
        }
        return $total;
    }
}

$bread = new \Cook\Ingredient('bread', 2.13);
$chicken = new \Cook\Ingredient('chicken', 3.20);
$sandwich = new ExEntree('sandwich', array($bread, $chicken));
echo "<pre>";
var_dump($sandwich);
echo "<br />";
echo $sandwich->getTotalCost();
