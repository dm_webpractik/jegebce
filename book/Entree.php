<?php
/**
 * Created by PhpStorm.
 * User: DM
 * Date: 12.06.2019
 * Time: 21:41
 */

class Entree
{
    public $name;
    public $ingredients = array();

    public function __construct($name, $ingredients){
        if (!is_array($ingredients)){
            throw new Exception('$ingredients must be an array');
        }
        $this->name = $name;
        $this->ingredients = $ingredients;
    }

    public function hasIngredients($ingredient){
        return in_array($ingredient, $this->ingredients);
    }
    public static function getSizes(){
        return array('small', 'medium', 'large');
    }
}