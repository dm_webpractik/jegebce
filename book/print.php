<?php
/**
 * Created by PhpStorm.
 * User: DM
 * Date: 12.06.2019
 * Time: 21:44
 */
require_once ('Entree.php');
require_once ('Ingredient.php');

class ComboMeal extends Entree {
    public function __construct($name, $entrees)
    {
        parent::__construct($name, $entrees);
        foreach ($entrees as $entree){
            if (!$entree instanceof Entree){
                throw new Exception('Elements of $entrees must be Entree objects');
            }

        }
    }

    public function hasIngredients($ingredient){
        foreach ($this->ingredients as $entree) {
            if ($entree->hasIngredients($ingredient)){
                return true;
            }
        }
        return false;
    }
}
class ExEntree extends Entree {
    public $ingredients;
    public function __construct($name, $ingredients)
    {
        $this->$ingredients = $ingredients;
        parent::__construct($name, $this->ingredients);
    }
    public function getTotalCost(){
        $total = 0;
        foreach ($this->ingredients as $ing) {
            $total += $ing->getPrice();
        }
        return $total;
    }
}
//$soup = new Entree('Chicken Soup', array('chicken','water'));
//$sandwich = new Entree('Chicken Sandwich', array('chicken','bread'));
//$combo = new ComboMeal('Soup + Sandwich', array($soup, $sandwich));

//foreach (['chicken', 'water', 'pickles'] as $ing) {
//    if ($combo->hasIngredients($ing)) {
//        echo "Something in the combo contains $ing <br>";
//    }
//}

$sizes = Entree::getSizes();

try {
    $drink = new Entree('Glass of milk', 'milk');
    if ($drink->hasIngredients('milk')) {
        echo "Yammy!";
    }
} catch (Exception $e) {
    echo "Couldn't create the drink: ". $e->getMessage();
}

$bread = new Ingredient('bread', 2.13);
$chicken = new Ingredient('chicken', 3.20);
$sandwich = new ExEntree('sandwich', array($bread, $chicken));
echo "<pre>";
var_dump($sandwich);
echo "<br />";
echo $sandwich->getTotalCost();