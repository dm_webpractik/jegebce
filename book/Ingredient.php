<?php
/**
 * Created by PhpStorm.
 * User: DM
 * Date: 19.06.2019
 * Time: 17:47
 */
namespace Cook;

class Ingredient
{
public $name;
private $price;
public function getPrice(){
    return $this->price;
}
public function changePrice($price){
    $this->price = $price;
}
public function __construct($name, $price)
{
    $this->name = $name;
    $this->price = $price;
}
}