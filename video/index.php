<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/header.php');?>
<div class="topper"></div>
<div class="top_header">
    <h1>VideoGalley</h1>
</div>
    <div class="content">
        <section>
            <div class="row">
                <div class="video-container">
                    <div class="video_content">
                        <video src="../upload/video/_Large.mp4" controls width="400">
                            Your browser doesn't support HTML5 video tag.
                        </video>
                    </div>
                </div>
                <div class="video-description">
                    <div class="itemName">
                        <span>Caption:</span>
                        <span>Rain in Shanghai</span>
                    </div>
                    <div class="itemDate">
                        <span>Date:</span>
                        <span>22-Jun-2009</span>
                    </div>
                    <div class="itemDescription">
                        <span>Description:</span>
                        <p>Description of video. This video I had created from photo The-Shanghai-World-Financial-Center-in-China-20170903.jpg.</p>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="video-description">
                    <div class="itemName">
                        <span>Caption:</span>
                        <span>Name of video</span>
                    </div>
                    <div class="itemDate">
                        <span>Date:</span>
                        <span>22-Jun-2009</span>
                    </div>
                    <div class="itemDescription">
                        <span>Description:</span>
                        <p>Description of picture. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. </p>
                    </div>
                </div>
                <div class="video-container">
                    <div class="video_content">
                        <video src="../upload/video/_Large.mp4" controls width="300" height="300" poster="/img/noPlay.png">
                            Your browser doesn't support HTML5 video tag.
                        </video>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="video-container">
                    <div class="video_content">
                        <video src="../upload/video/_Large.mp4" controls width="400" height="400" poster="/img/noPlay.png">
                            Your browser doesn't support HTML5 video tag.
                        </video>
                    </div>
                </div>
                <div class="video-description">
                    <div class="itemName">
                        <span>Caption:</span>
                        <span>Name of video</span>
                    </div>
                    <div class="itemDate">
                        <span>Date:</span>
                        <span>22-Jun-2009</span>
                    </div>
                    <div class="itemDescription">
                        <span>Description:</span>
                        <p>Description of picture. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="video-description">
                    <div class="itemName">
                        <span>Caption:</span>
                        <span>Name of video</span>
                    </div>
                    <div class="itemDate">
                        <span>Date:</span>
                        <span>22-Jun-2009</span>
                    </div>
                    <div class="itemDescription">
                        <span>Description:</span>
                        <p>Description of picture. It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. </p>
                    </div>
                </div>
                <div class="video-container">
                    <div class="video_content">
                        <video src="../upload/video/_Large.mp4" controls width="300" height="300" poster="/img/noPlay.png">
                            Your browser doesn't support HTML5 video tag.
                        </video>
                    </div>
                </div>
            </div>
        </section>
        <div class="clear"></div>
    </div>
<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/footer.php');?>
