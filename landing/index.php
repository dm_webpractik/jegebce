<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="New barbershop Irina beard cutter" />
    <meta name="keywords" content="barber, Irina, New barbershop " />
    <title>New landing for =Irina beard cutter=</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="container">
    <div class="header">
        <div class="green_circle"></div>
        <div class="top_text">Cutting beards</div>
    </div>
    <div class="h5span">
        <h5>
            <span>Our website is coming soon</span>
        </h5>
    </div>
    <div class="h3span">
        <h3>
            <span>This text should be replaced with information about you and your business This text should be replaced with information about you and your business This text should be replaced with information about you and your...
            </span>
        </h3>
    </div>
</div>
<div>
    <div class="footer">
        <div class="contacts">
            <h2>
                <span>Contact us</span>
            </h2>
        </div>
        <div class="first_column">
            <p class="wb-stl-normal"><span class="wb-stl-highlight"><strong><span class="wb_tr_ok">Address:</span></strong> </span></p>
            <p class="wb-stl-normal"><span class="wb-stl-highlight"><span class="wb_tr_ok">Shiny Shop</span></span></p>
            <p class="wb-stl-normal"><span class="wb-stl-highlight"><span class="wb_tr_ok">Dobrovolskogo, 15</span></span></p>
            <p class="wb-stl-normal"><span class="wb-stl-highlight"><span class="wb_tr_ok">Rostov-on-Don</span></span></p>
            <p class="wb-stl-normal"><span class="wb-stl-highlight"><span class="wb_tr_ok">344000</span></span></p>
        </div>
        <div class="second_column">
            <p class="wb-stl-normal"><strong><span class="wb-stl-highlight"><span class="wb_tr_ok">Phone:</span></span></strong></p>
            <p class="wb-stl-normal"><span class="wb-stl-highlight"><span style='color: rgb(255, 255, 255); font-family: "Trebuchet MS", sans-serif; font-size: 16px; background-color: transparent;'><span class="wb_tr_ok"><span dir="ltr" style="direction: ltr;">+7 938 103 6851</span></span></span></span></p>
            <p class="wb-stl-normal"><span class="wb-stl-highlight"><span class="wb_tr_ok">E-mail:</span> </span></p>
            <p class="wb-stl-normal"><span class="wb-stl-highlight">ira4kamoor@gmail.com</span></p>
        </div>
    </div>
    <div class="wb_cont_outer"></div>
    <div class="wb_cont_bg"></div>
</div>
<div class="wb_sbg"></div>
</body>
</html>
