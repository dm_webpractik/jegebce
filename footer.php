</div>

</header>
<footer id="footer">
    <div class="bottommenu">
        <ul>
            <?php
            foreach ($arBottonMenuItems as $id=>$menuItem) {
                echo "<li><a";
                echo " href=".$menuItem[1]."><span>$menuItem[0]</span></a></li>";
            } ?>
        </ul>
    </div>
    <div class="copy">
        <span>&copy; <?php echo date('Y');?> DEEM MOOR</span>
    </div>
    <div class="ya-iks">
        <a href="https://webmaster.yandex.ru/siteinfo/?site=https://jegebce.online"><img width="60" height="20" alt="IKS" border="0" src="https://yandex.ru/cycounter?https://jegebce.online&theme=dark&lang=en"/></a>
    </div>
 <div class="up_arrow_container">
     <div class="up_arrow_bg">
         <div class="up_arrow">
             <i class="fas fa-arrow-up"></i>
         </div>
     </div>

 </div>
</footer>
</div>
<script src="/js/jquery-3.3.1.js"></script>
<script src="/js/jquery.fancybox.js"></script>
<script src="/js/main.js"></script>
<script>
    let hiddenBox2 = $("div.up_arrow_container");
    $( window ). scroll( function() {
        if ($( window ). scrollTop() > 10)
        hiddenBox2.show();
        else hiddenBox2.hide();
    });

    $("div.up_arrow_container").click(function() {
        $('body,html').animate({scrollTop:0},500);
    });
    $(".search input").on("focus", function () {
        $(".search").animate({width:320},500);
    });
    $(".search input").on("blur", function () {
        $(".search").animate({width:220},500);
    })
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50101867 = new Ya.Metrika2({
                    id:50101867,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50101867" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>