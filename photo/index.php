<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/header.php');?>
<div class="topper"></div>
<div class="top_header">
    <h1>PhotoGalley</h1>
</div>
<div class="content">
    <section>
        <div class="row">
            <div class="photo-container">
                <a data-fancybox data-animation-duration="700" data-options='{"caption" : "My first digital photo"}' data-src="/upload/photo/x_227f5f5d.jpg" href="javascript:;">
                    <img src="/upload/photo/x_227f5f5d.jpg" alt="no picture">
                </a>
            </div>
            <div class="photo-description">
                <div class="photoName">
                    <span>Caption:</span>
                    <span>My first digital photo</span>
                </div>
                <div class="photoDate">
                    <span>Date:</span>
                    <span>22-Jun-1995</span>
                </div>
                <div class="photoDesc">
                    <span>Description:</span>
                    <p>This picture was taken at my home when Andrew had bought his first digital camera Kodak DC40.</p>
                </div>
            </div>
            <div class="mobile-photo-description" hidden>
                <div class="mobile-photoName">
                    <span>My first digital photo</span>
                </div>
                <div class="mobile-photoDate">
                    <span>22/06/1995</span>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="photo-description">
                <div class="photoName">
                    <span>Caption:</span>
                    <span>JEGEBCE Studio</span>
                </div>
                <div class="photoDate">
                    <span>Date:</span>
                    <span>22-Jun-2009</span>
                </div>
                <div class="photoDesc">
                    <span>Description:</span>
                    <p>JEGEBCE Studio in deal. Recording new song...</p>
                </div>
            </div>
            <div class="mobile-photo-description" hidden>
                <div class="mobile-photoName">
                    <span>JEGEBCE Studio</span>
                </div>
                <div class="mobile-photoDate">
                    <span>22/Jul/1996</span>
                </div>

            </div>
            <div class="photo-container">
                <a data-fancybox data-src="/upload/photo/x_dd8531fa.jpg" href="javascript:;">
                    <img src="/upload/photo/x_dd8531fa.jpg" alt="no picture">
                </a>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="photo-container">
                <a data-fancybox data-src="/upload/photo/x_a7bffb45.jpg" href="javascript:;">
                    <img src="/upload/photo/x_a7bffb45.jpg" alt="no picture">
                </a>
            </div>
            <div class="photo-description">
                <div class="photoName">
                    <span>Caption:</span>
                    <span>In "Upper Bar"</span>
                </div>
                <div class="photoDate">
                    <span>Date:</span>
                    <span>22-Mon-2009</span>
                </div>
                <div class="photoDesc">
                    <span>Description:</span>
                    <p>Limanchik. Performing in "Upper Bar".</p>
                </div>
            </div>
            <div class="mobile-photo-description" hidden>
                <div class="mobile-photoName">
                    <span>Limanchik. In "The Upper Bar"</span>
                </div>
                <div class="mobile-photoDate">
                    <span>22-Jun-2009</span>
                </div>

            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="photo-description">
                <div class="photoName">
                    <span>Caption:</span>
                    <span>At our's camp</span>
                </div>
                <div class="photoDate">
                    <span>Date:</span>
                    <span>22-Jun-2009</span>
                </div>
                <div class="photoDesc">
                    <span>Description:</span>
                    <p>Repetition before performing in "Upper Bar"</p>
                </div>
            </div>
            <div class="mobile-photo-description" hidden>
                <div class="mobile-photoName">
                    <span>Limanchik. At our's camp</span>
                </div>
                <div class="mobile-photoDate">
                    <span>22-Jun-2009</span>
                </div>
            </div>
            <div class="photo-container">
                <a data-fancybox data-src="/upload/photo/x_b079bb11.jpg" href="javascript:;">
                    <img src="/upload/photo/x_b079bb11.jpg" alt="no picture">
                </a>
            </div>
        </div>
    </section>
    <div class="clear"></div>
</div>
<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/footer.php');?>
