<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>
<div class="topper"></div>
<div class="top_header">
    <h1>Audio</h1>
</div>
<div class="content">
    <section>
        <div class="row" itemscope itemtype="http://schema.org/AudioObject">
            <div class="audio-container" >
                <img src="/img/nature.jpg" alt="Nature">
                    <div class="audio_content">
                        <meta itemprop="encodingFormat" content="audio/mpeg" />
                        <audio itemprop="contentUrl" src="/upload/audio/bitrix_tune.mp3" controls>
                            Your browser does not support the
                            <code>audio</code> element.
                        </audio>
                    </div>
            </div>
            <div class="audio-description">
                <div class="itemName">
                    <span>Track:</span>
                    <span itemprop="name">Bitrix tune</span>
                </div>
                <div class="itemName">
                    <span>Artist:</span>
                    <span>Unknown</span>
                </div>
                <div class="itemName">
                    <span>Album:</span>
                    <span>Unleashed</span>
                </div>
                <div class="itemDate">
                    <span>Year:</span>
                    <span>2018</span>
                </div>
                <div class="itemDate">
                    <span>Duration:</span>
                    <span itemprop="duration">0:04</span>
                </div>
                <div class="itemDescription">
                    <span>Description:</span>
                    <p itemprop="description">Description of track. I had downloaded this track for example from Bitrix site.</p>
                </div>
            </div>
            <div class="mobile-audio-description" hidden>
                <div class="mobile-itemName">
                    <span>Unknown</span> /
                    <span>Bitrix tune</span>
                </div>
                <div class="mobile-itemName">
                    <span>Unleashed</span> /
                    <span>2018</span>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row" itemscope itemtype="http://schema.org/AudioObject">
            <div class="audio-description">
                <div class="itemName">
                    <span>Track:</span>
                    <span itemprop="name">Light Blue Jacket</span>
                </div>
                <div class="itemName">
                    <span>Artist:</span>
                    <span itemprop="author">DM / JEGEBCE</span>
                </div>
                <div class="itemName">
                    <span>Album:</span>
                    <span>15</span>
                </div>
                <div class="itemDate">
                    <span>Year:</span>
                    <span>2008</span>
                </div>
                <div class="itemDate">
                    <span>Duration:</span>
                    <span itemprop="duration">1:58</span>
                </div>
                <div class="itemDescription">
                    <span>Description:</span>
                    <p itemprop="description" contenteditable="true" spellcheck="false">Remastered in 2008 song from first album "Best of the JEGEBCE" (1993). Was written for 15 years anniversary of rock-group.</p>
                </div>
            </div>
            <div class="mobile-audio-description" hidden>
                <div class="mobile-itemName">
                    <span>DM</span> /
                    <span>Light Blue Jacket</span>
                </div>
                <div class="mobile-itemName">
                    <span>15</span> /
                    <span>2008</span>
                </div>
            </div>
            <div class="audio-container">
                <img src="/img/noPlay.png" alt="no picture" draggable="true">
                <div class="audio_content">
                    <meta itemprop="encodingFormat" content="audio/mpeg" />
                    <audio itemprop="contentUrl" src="/upload/audio/Blue jacket.mp3" controls>
                        Your browser does not support the
                        <code>audio</code> element.
                    </audio>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row" itemscope itemtype="http://schema.org/AudioObject">
            <div class="audio-container">
                <img src="/img/noPlay.png" alt="no picture">
                <div class="audio_content">
                    <meta itemprop="encodingFormat" content="audio/mpeg" />
                    <audio itemprop="contentUrl" src="/upload/audio/world of loneliness.mp3" controls>
                        Your browser does not support the
                        <code>audio</code> element.
                    </audio>
                </div>
            </div>
            <div class="audio-description">
                <div class="itemName">
                    <span>Track:</span>
                    <span itemprop="name">The World of Loneliness</span>
                </div>
                <div class="itemName">
                    <span>Artist:</span>
                    <span itemprop="author">DM</span>
                </div>
                <div class="itemName">
                    <span>Album:</span>
                    <span>Album name</span>
                </div>
                <div class="itemDate">
                    <span>Year:</span>
                    <span>2009</span>
                </div>
                <div class="itemDate">
                    <span>Duration:</span>
                    <span itemprop="duration">1:45</span>
                </div>
                <div class="itemDescription">
                    <span>Description:</span>
                    <p itemprop="description">The song from last recorded album.</p>
                </div>
            </div>
            <div class="mobile-audio-description" hidden>
                <div class="mobile-itemName">
                    <span>DM</span> /
                    <span>The World of Loneliness</span>
                </div>
                <div class="mobile-itemName">
                    <span>Album name</span> /
                    <span>2009</span>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="audio-description">
                <div class="itemName">
                    <span>Track:</span>
                    <span>Tl</span>
                </div>
                <div class="itemName">
                    <span>Artist:</span>
                    <span>DM</span>
                </div>
                <div class="itemName">
                    <span>Album:</span>
                    <span>Single</span>
                </div>
                <div class="itemDate">
                    <span>Year:</span>
                    <span>2001</span>
                </div>
                <div class="itemDate">
                    <span>Duration:</span>
                    <span>1:56</span>
                </div>
                <div class="itemDescription">
                    <span>Description:</span>
                    <p>First electronic composition written by myself in early 2000th</p>
                </div>
            </div>
            <div class="mobile-audio-description" hidden>
                <div class="mobile-itemName">
                    <span>DM</span> /
                    <span>Tl</span>
                </div>
                <div class="mobile-itemName">
                    <span>Single</span> /
                    <span>2001</span>
                </div>
            </div>
            <div class="audio-container">
                <img src="/img/noPlay.png" alt="no picture">
                <div class="audio_content">
                    <audio src="/upload/audio/Tl.mp3" controls>
                        Your browser does not support the
                        <code>audio</code> element.
                    </audio>
                </div>
            </div>
        </div>
    </section>
    <div class="clear"></div>
</div>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>
