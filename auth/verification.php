<?php require($_SERVER['DOCUMENT_ROOT']."/admin/init.php");
function generateCode($length=6) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0,$clen)];
    }
    return $code;
}

if (isset($_POST["SUBMIT_BUTTON"])) {

    $errors = [];

    if (empty($_POST["USER_LOGIN"])) $errors[] = "Login is empty";
    if (empty($_POST["USER_PASSWORD"])) $errors[] = "Password is empty";

    $result = GetUser::getIdbyLogin($_POST["USER_LOGIN"]);

    if (empty($result['id'])) $errors[] = "User <b>" . $_POST["USER_LOGIN"] . "</b> not found";

    elseif ($result['password'] != $_POST["USER_PASSWORD"]) $errors[] = "Password for <b>" . $_POST["USER_LOGIN"] . "</b> is incorrect. Try again!";

    if (count($errors) == 0) {
        $hash = md5(generateCode(10));
        $ip = ip2long($_SERVER['REMOTE_ADDR']);
        $flag = GetUser::updateIpHashbyId($ip, $hash, $result['id']);
        if ($flag) {
            setcookie("id", $result["id"], time() + 60 * 60 * 24 * 30, "/");
            setcookie("hash", $hash, time() + 60 * 60 * 24 * 30, "/", false, false, true);
            setcookie("identification", "jegebce.online.authentic", time() + 60 * 60 * 24 * 30,"/");
            session_start();
            $_SESSION['login'] = $_POST["USER_LOGIN"];
            header("Location: check.php");
            die();
        }
    }
    else {
        echo "Found trouble count: ".count($errors)."<br>";
        foreach ($errors as $error) echo $error . "<br>";
        echo "<br><a href='/auth'>back</a>";
    }


}

//mysqli_close($link);
