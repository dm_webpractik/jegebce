<?php require($_SERVER['DOCUMENT_ROOT']."/admin/init.php");
$secret = '6LcZpMAUAAAAAFX1Zr3eMb0Ba9XZSLQKz2lycEuQ';
$filename = $_SERVER["DOCUMENT_ROOT"]."/recapturelogs.log";
function generateCode($length = 6)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0, $clen)];
    }
    return $code;
}

if (isset($_POST["SUBMIT_BUTTON"])&& $_REQUEST["response"] != '') {
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_REQUEST["response"]);
    $responseData = json_decode($verifyResponse, true);
    if (file_exists($filename)) {
        $log_txt = "\r\nLOGIN || \r\n";
        foreach ($responseData as $key=>$value) {
            if (is_array($value)) {
                $log_txt .= "\r\nKEY: $key; VALUES:\r\n";
                foreach ($value as $items) {
                    $log_txt .= "- $items;\r\n";
                }
            }
            else {
                $log_txt .= "\r\nKEY: $key; VALUE: $value";
            }
        }
        $log_txt .= "\r\n";
        $handle = fopen($filename, "a");
        fwrite($handle, $log_txt);
        fclose($handle);
    }
    $errors = [];
    if (empty($_POST["USER_LOGIN"])) $errors[] = "Login is empty";
    if (empty($_POST["USER_PASSWORD"])) $errors[] = "Password is empty";
    $result = GetUser::getIdbyLogin($_POST["USER_LOGIN"]);
    if (empty($result['id'])) $errors[] = "User <b style='color: white'>" . $_POST["USER_LOGIN"] . "</b> not found";
    elseif ($result['password'] != $_POST["USER_PASSWORD"]) $errors[] = "Password for <b style='color: white'>" . $_POST["USER_LOGIN"] . "</b> is incorrect. Try again!";
    if (count($errors) == 0 && $responseData["score"] > 0.3) {
        $hash = md5(generateCode(10));
        $ip = ip2long($_SERVER['REMOTE_ADDR']);
        $flag = GetUser::updateIpHashbyId($ip, $hash, $result['id']);
        if ($flag) {
            setcookie("id", $result["id"], time() + 60 * 60 * 24 * 30, "/", 'jegebce.online', true, false);
            setcookie("hash", $hash, time() + 60 * 60 * 24 * 30, "/", 'jegebce.online', true, false);
            setcookie("identification", "jegebce.online.authentic", time() + 60 * 60 * 24 * 30,"/", 'jegebce.online', true, true);
            session_start();
            $_SESSION['login'] = $_POST["USER_LOGIN"];
            header("Location: check.php");
            die();
        }
    }
    else {
            $str_errors = "";
            foreach ($errors as $error) {
                $str_errors .= $error ."<br>";
            }
            $_POST["ERROR"] = $str_errors;
    }
}
include_once($_SERVER['DOCUMENT_ROOT'] . '/header.php');
?>
<div class="auth_div">
    <div class="shadow"></div>
    <h1>Authorization</h1>
    <form name="auth_form" method="post" action="index.php">
        <label class="form-group">
            <span class="form-group__name">Login:</span>
            <input name="USER_LOGIN" type="text" placeholder="Login" maxlength="20" required>
        </label>
        <label class="form-group">
            <span class="form-group__name">Password:</span>
            <input name="USER_PASSWORD" type="password" placeholder="Password" maxlength="20" required>
        </label>
        <button class="button button--primary" name="SUBMIT_BUTTON" id="btn_sub"><span>ENTER</span></button>
        <div class="g-recaptcha" data-sitekey=""></div>
        <input type="hidden" name="response" id="h_i_res">
    </form>
    <div id="example3"></div>
    <div class="auth_text">
        <span class="tinytext">First time at jegebce.online?</span>
        <span class="tinytext">
                <a href="/auth/register.php">Registration</a>
            </span>
    </div>
    <div class="errors_tab">
        <div class="error_text">
            &nbsp;
            <? if (isset($_POST["ERROR"])) echo $_POST["ERROR"]; ?>
        </div>
    </div>
</div>
<script>
    grecaptcha.ready(function() {
        // grecaptcha.render('example3', {
        //     'sitekey' : '6LcZpMAUAAAAAEKGsW54zpyhxQeHcLGJd-5kurlZ',
        //     'theme' : 'dark'
        // });
        grecaptcha.execute('6LcZpMAUAAAAAEKGsW54zpyhxQeHcLGJd-5kurlZ', {action: 'homepage'}).then(function(token) {
            $("#h_i_res").val(token);
        });
    });
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>
