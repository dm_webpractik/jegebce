<?php require($_SERVER['DOCUMENT_ROOT']."/admin/init.php");
$to = "deemmoor@yandex.ru";
$from   = "admin@jegebce.online";
$headers  = "From: " . strip_tags($from) . "\r\n";
$headers .= "Reply-To: ". strip_tags($from) . "\r\n";
$headers .= "MIME-Version: 1.0"."\r\n";
$headers .= "Content-Type: text/html;charset=utf-8"."\r\n";
$subject = "Авторизация на сайте jegebce.online";
$mailmessage = "<h2>Привет, Администратор!</h2><br><b>На <a href='http://jegebce.online'>сайте</a></b> обнаружина активность, следующего характера:<br>";
$time = strtotime('+3 hours');
$filename = $_SERVER["DOCUMENT_ROOT"]."/logins.log";
if (isset($_COOKIE['id']) and isset($_COOKIE['hash']))
{
    $result = GetUser::getAllDataById($_COOKIE['id']);
    if(($result['hash'] !== $_COOKIE['hash']) or ($result['id'] !== $_COOKIE['id']) or ($result['ip'] !== $_SERVER['REMOTE_ADDR'])  and ($result['ip'] !== "0"))
    {
        setcookie("id", "", time() - 3600*24*30*12, "/");
        setcookie("hash", "", time() - 3600*24*30*12, "/");
        echo "Oops, some problems detected...<br>";
        echo "Hash: " . $result['hash'] . " = " . $_COOKIE['hash'] . "<br>" ;
        echo "ID: " . $result['id'] . " = " . $_COOKIE['id'] . "<br>" ;
        echo "IP: " . $result['ip'] . " = " . $_SERVER['REMOTE_ADDR'] . "<br>" ;
        echo "User agent:". $_SERVER['HTTP_USER_AGENT']."<br>";
        echo "<br><a href='/auth'>back</a>";
    }
    else
    {
        $message = $result['name'] . " " . $result['surname'];
        $message = date("d-m-Y H:i:s", $time) . " The user (". $result['login'] .") " . $message . " had logged in." . " IP: " . $result['ip'] . "\r\n";
        if (file_exists($filename)) {
            $handle = fopen($filename, "a");
            fwrite($handle, $message);
            fclose($handle);
        }
        else $message .= "\r\n". "But logging failed.";
        $mailmessage .= $message;
        mail($to,$subject,$mailmessage,$headers);
        header("Location: /index.php");
    }
}
else
{
    echo "Cookies off. Turn it on.";
}
