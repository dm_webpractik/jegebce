<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22.10.2018
 * Time: 11:28
 */

class GetUser
{
    static private $host = "localhost";
    static private $dbuser = "deemmoor_nahu";
    static private $dbpassword = "123456";
    static private $database = "deemmoor_nahu";


    public static function getNameById($id) {
        $dblink = mysqli_connect(self::$host, self::$dbuser, self::$dbpassword, self::$database);
        if (mysqli_connect_errno()) {
            printf("Error: %s\n", mysqli_connect_error());
            echo "<br><a href='/'>back</a>";
            die();
        }
        $query = mysqli_query($dblink, "SELECT name, surname FROM users_db WHERE id = '" . intval($id) . "' LIMIT 1");
        $result = mysqli_fetch_assoc($query);
        mysqli_close($dblink);
        return $result['name'] . " " . $result['surname'];
    }

    public static function getLoginById($id) {
        $dblink = mysqli_connect(self::$host, self::$dbuser, self::$dbpassword, self::$database);
        if (mysqli_connect_errno()) {
            printf("Error: %s\n", mysqli_connect_error());
            echo "<br><a href='/'>back</a>";
            die();
        }
        $query = mysqli_query($dblink, "SELECT login FROM users_db WHERE id = '" . intval($id) . "' LIMIT 1");
        $result = mysqli_fetch_assoc($query);
        mysqli_close($dblink);
        return $result['login'];
    }

    public static function getAllDataById($id) {
        $arResult = array();
        $dblink = mysqli_connect(self::$host, self::$dbuser, self::$dbpassword, self::$database);
        if (mysqli_connect_errno()) {
            printf("Error: %s\n", mysqli_connect_error());
            echo "<br><a href='/'>back</a>";
            die();
        }
        $query = mysqli_query($dblink, "SELECT * FROM users_db WHERE id = '" . intval($id) . "' LIMIT 1");
        $result = mysqli_fetch_assoc($query);
        $result['ip'] = long2ip($result['ip']);
        mysqli_close($dblink);
        return $result;
    }

    public static function getIdbyLogin($login) {
        $dblink = mysqli_connect(self::$host, self::$dbuser, self::$dbpassword, self::$database);
        if (mysqli_connect_errno()) {
            printf("Error: %s\n", mysqli_connect_error());
            echo "<br><a href='/'>back</a>";
            die();
        }
        $query = mysqli_query($dblink, "SELECT id, password FROM users_db WHERE login='" . $login . "'");
        $result = mysqli_fetch_assoc($query);
        mysqli_close($dblink);
        return $result;
    }

    public static function updateIpHashbyId($ip,$hash,$id){
        $dblink = mysqli_connect(self::$host, self::$dbuser, self::$dbpassword, self::$database);
        if (mysqli_connect_errno()) {
            printf("Error: %s\n", mysqli_connect_error());
            echo "<br><a href='/'>back</a>";
            return false;
        }
        mysqli_query($dblink,"UPDATE users_db SET hash='".$hash."', ip='".$ip."' WHERE id='".$id."'");
        mysqli_close($dblink);
            return true;
    }
}
