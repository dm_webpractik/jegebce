<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/header.php');
$secret = '6LcZpMAUAAAAAFX1Zr3eMb0Ba9XZSLQKz2lycEuQ';
$filenamer = $_SERVER["DOCUMENT_ROOT"]."/recapturelogs.log";
$link = mysqli_connect($host, $dbuser, $dbpassword, $database);
$to = "deemmoor@yandex.ru";
$from   = "admin@jegebce.online";
$headers  = "From: " . strip_tags($from) . "\r\n";
$headers .= "Reply-To: ". strip_tags($from) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html;charset=utf-8 \r\n";
$subject = "Регистрация на сайте jegebce.online";
$time = strtotime('+3 hours');
$filename = $_SERVER["DOCUMENT_ROOT"]."/logins.log";
if (mysqli_connect_errno()) {
    printf("SQL Server return: %s\n", mysqli_connect_error());
    echo "<br><a href='/'>back</a><br>";
    die("SQL down");
}
if (isset($_POST["SUBMIT_BUTTON"]) && $_REQUEST["response"] != '') {
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_REQUEST["response"]);
    $responseData = json_decode($verifyResponse, true);
    if (file_exists($filenamer)) {
        $log_txt = "\r\nREGISTER || \r\n";
        foreach ($responseData as $key=>$value) {
            if (is_array($value)) {
                $log_txt .= "\r\nKEY: $key; VALUES:\r\n";
                foreach ($value as $items) {
                    $log_txt .= "- $items;\r\n";
                }
            }
            else {
                $log_txt .= "\r\nKEY: $key; VALUE: $value";
            }
        }
        $log_txt .= "\r\n";
        $handle = fopen($filenamer, "a");
        fwrite($handle, $log_txt);
        fclose($handle);
    }
    $errors = [];
        if ($_POST["USER_NAME"] == $_POST["USER_SURNAME"]) $errors[] = "Name and surname are the same, try use different";
        if (!preg_match("/^[a-zA-Z0-9]+$/", $_POST["USER_LOGIN"] )) $errors[] = "Use english alphabet and digits for login only";
        if (strlen($_POST["USER_LOGIN"]) < 3 or strlen($_POST["USER_LOGIN"]) > 20) $errors[] = "Login is less then 3 letters";
    $query = mysqli_query($link, "SELECT id FROM users_db WHERE login='" . $_POST["USER_LOGIN"] . "'");
        if (mysqli_num_rows($query) > 0) $errors[] = "User with same login is exist";
        $ip = ip2long($_SERVER['REMOTE_ADDR']);
        $q = mysqli_query($link, "SELECT memo FROM black_list WHERE ip='" .$ip . "'");
        if (mysqli_num_rows($q) > 0) {
            $result = mysqli_fetch_assoc($q);
            $errors[] = "You are blocked, 'cause you are ". $q['MEMO'];
        }
        if ($responseData["score"] < 0.4) {
            $errors[] = "Most possible you are bot!";
            $subject = "Бот рвётся на сайте jegebce.online";
            $message = date("d-m-Y H:i:s", $time) . " The user ". $_POST["USER_LOGIN"] ." definitely is bot!!!. IP Address: ". long2ip($ip);
            mail($to,$subject,$message,$headers);
        }
        if (count($errors) == 0 && $responseData["success"]) {
            $login = $_POST["USER_LOGIN"];
            $password = trim($_POST["USER_PASSWORD"]);
            $email = $_POST["USER_EMAIL"];
            $name = $_POST["USER_NAME"];
            $surname = $_POST["USER_SURNAME"];

            mysqli_query($link, "INSERT INTO users_db SET login='".$login."', password='".$password."', email='".$email."', name='".$name."', surname='".$surname."', active='Y', ip='".$ip."'");
            if (!mysqli_connect_errno()) {
                echo "Registration successfully completed. Welcome to jegebce.online!";
                $message= date("d-m-Y H:i:s", $time) . " The user ". $login ." had registered. IP Address: ". long2ip($ip);
                mail($to,$subject,$message,$headers);
                echo "<meta http-equiv=\"refresh\" content=\"1;url=index.php\">";
            }
            else echo "SQL Server skunk...";
            die();
        }
        else {
            echo "Here some trouble:<br>";
            foreach ($errors as $error)
                echo $error . "<br>";
        }
}?>
<div class="auth_div">
    <h3>Registration</h3>
    <form id="reg_form" name="reg_form" method="post">
        <label class="form-group">
            <span class="form-group__name">Login:</span>
            <input name="USER_LOGIN" type="text" placeholder="Login" maxlength="20" required>
        </label>
        <label class="form-group">
            <span class="form-group__name">Password:</span>
            <input name="USER_PASSWORD" type="password" placeholder="Password" maxlength="20" required>
        </label>
        <label class="form-group">
            <span class="form-group__name">E-mail:</span>
            <input name="USER_EMAIL" type="email" placeholder="user@mail.ru" maxlength="20">
        </label>
        <label class="form-group">
            <span class="form-group__name">Name:</span>
            <input name="USER_NAME" type="text" placeholder="John" maxlength="20">
        </label>
        <label class="form-group">
            <span class="form-group__name">Surname:</span>
            <input name="USER_SURNAME" type="text" placeholder="Lennon" maxlength="20">
        </label>
        <label class="check">
            <input name="AGREE" type="checkbox" checked>
            <span class="tinytext"> I agree with <a href="">rules</a> </span>
        </label>
        <button class="button button--primary" name="SUBMIT_BUTTON" id="btn_sub"><span>Register</span></button>
        <input type="hidden" name="response" id="h_i_res">
    </form>
    <div>
        <div class="auth_text">
            <span class="tinytext">
                Some shit about your personal data...
            </span>
        </div>
    </div>
</div>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LcZpMAUAAAAAEKGsW54zpyhxQeHcLGJd-5kurlZ', {action: 'homepage'}).then(function(token) {
            $("#h_i_res").val(token);
        });
    });
</script>
<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/footer.php');?>
