buttonSecretSelector.click(function (event) {
    event.preventDefault();
    event.stopPropagation();
    buttonSecretSelector.attr("disabled", true);
    var secretCode = secretInputSelector.val();
    if (secretCode && secretCode.length > 0) {
        $.ajax({
            type: "post",
            url: "/ru/authcode",
            data: {
                fqdn: domainInputSelector.val(),
                method: $("#domain-authcode-method").val(),
                confirmation_code: secretCode,
                captcha_code: recaptchaCode
            },
            success: function (data, textStatus, xhr) {
                if (xhr.status === 200) {
                    if (data && data.result && data.result === 'success') {
                        showSuccess();
                    } else {
                        if (data && data.result) {
                            switch (data.result) {
                                case "confirm_code_incorrect":
                                    if (isEnLocal) {
                                        setSecretErrorText("You entered an invalid verification code");
                                        break;
                                    }
                                    setSecretErrorText("аб аВаВаЕаЛаИ аНаЕ аВаЕбаНбаЙ аКаОаД аПаОаДбаВаЕбаЖаДаЕаНаИб");
                                    break;
                                case "cannot_send_code":
                                    if (isEnLocal) {
                                        setSecretErrorText("Error sending code");
                                        break;
                                    }
                                    setSecretErrorText("абаИаБаКаА аОбаПбаАаВаКаИ аКаОаДаА");
                                    break;
                                case "unknown":
                                    if (isEnLocal) {
                                        setSecretErrorText("An unknown error occurred during the operation. Please refresh the page and try again or contact the support service.");
                                        break;
                                    }
                                    setSecretErrorText("а баОаДаЕ аОаПаЕбаАбаИаИ аПбаОаИаЗаОбаЛаА аНаЕаИаЗаВаЕббаНаАб аОбаИаБаКаА. ааБаНаОаВаИбаЕ бббаАаНаИбб аИ аПаОаПбаОаБбаЙбаЕ аЕбаЕ баАаЗ аИаЛаИ аОаБбаАбаИбаЕбб аВ баЛбаЖаБб аПаОаДаДбаЖаКаИ.");
                                    break;
                            }
                        } else {
                            if (isEnLocal) {
                                setSecretErrorText("An unknown error occurred during the operation. Please refresh the page and try again or contact the support service.");
                            } else {
                                setSecretErrorText("а баОаДаЕ аОаПаЕбаАбаИаИ аПбаОаИаЗаОбаЛаА аНаЕаИаЗаВаЕббаНаАб аОбаИаБаКаА. ааБаНаОаВаИбаЕ бббаАаНаИбб аИ аПаОаПбаОаБбаЙбаЕ аЕбаЕ баАаЗ аИаЛаИ аОаБбаАбаИбаЕбб аВ баЛбаЖаБб аПаОаДаДбаЖаКаИ.");
                            }
                        }
                    }
                } else {
                    if (isEnLocal) {
                        setSecretErrorText("An unknown error occurred during the operation. Please refresh the page and try again or contact the support service.");
                    } else {
                        setSecretErrorText("а баОаДаЕ аОаПаЕбаАбаИаИ аПбаОаИаЗаОбаЛаА аНаЕаИаЗаВаЕббаНаАб аОбаИаБаКаА. ааБаНаОаВаИбаЕ бббаАаНаИбб аИ аПаОаПбаОаБбаЙбаЕ аЕбаЕ баАаЗ аИаЛаИ аОаБбаАбаИбаЕбб аВ баЛбаЖаБб аПаОаДаДбаЖаКаИ.");
                    }
                }
            },
            error: function () {
                if (isEnLocal) {
                    setSecretErrorText("An unknown error occurred during the operation. Please refresh the page and try again or contact the support service.");
                } else {
                    setSecretErrorText("а баОаДаЕ аОаПаЕбаАбаИаИ аПбаОаИаЗаОбаЛаА аНаЕаИаЗаВаЕббаНаАб аОбаИаБаКаА. ааБаНаОаВаИбаЕ бббаАаНаИбб аИ аПаОаПбаОаБбаЙбаЕ аЕбаЕ баАаЗ аИаЛаИ аОаБбаАбаИбаЕбб аВ баЛбаЖаБб аПаОаДаДаЕбаЖаКаИ.");
                }
            },
            dataType: 'json'
        });
    } else {
        if (isEnLocal) {
            setSecretErrorText("Enter the secret code to get AuthInfo-code")

        } else {
            setSecretErrorText("ааВаЕаДаИбаЕ баЕаКбаЕбаНбаЙ аКаОаД аДаЛб аПаОаЛббаЕаНаИб AuthInfo-code")
        }
    }
});
