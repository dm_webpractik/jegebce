<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/header.php');?>
<div class="topper"></div>
<div class="top_header">
    <h1>Search results</h1>
</div>
<div class="content">
    <section>
        <div class="found_content">
<?if (isset($_GET["searchtext"]) && !empty($_GET["searchtext"])) {
    $bSearchResult = false;
    $folder = $_SERVER['DOCUMENT_ROOT'];
    $str_to_find = $_GET["searchtext"];
    if ($handle = opendir($folder)) {
        echo "Search result for <b>" . $str_to_find . "</b>: <br>";
        while (false !== ($file = readdir($handle))) {
            if (!is_dir($folder . '/' . $file)) {
                $str_content = file_get_contents($folder . '/' . $file);
                $pos = strpos($str_content, $str_to_find);
                if ($pos !== false) {
                    echo "<br>Found in file <a href='../" . $file . "' target='_blank'>" . $file . "</a> at " . $pos . " string: <br>";
                    echo "..." . substr($str_content, $pos, 20) . "...<br>";
                    $bSearchResult = true;
                }
            }
        }
        if (!$bSearchResult) echo "Not found";
        closedir($handle);
    }
}
else echo "Empty request";
echo "<br><a href='/'>back</a><br>";?>
        </div>
    <div class="clear"></div>
</div>
<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/footer.php');?>
