<?php
function calc($date) {
    list($day, $month, $year) =  explode(".", $date);
    if ( $month > 2 ) {
        $month += 1;
    }
    else {
        $month += 13; $year -= 1;
    }
    $month = ceil(30.6*$month);
    $year = ceil(365.25*$year);
  return $year+$month+$day;
 }
 $date = $_REQUEST["date"];
 $today = date("d.m.Y");
 $diff = abs(calc($today)-calc($date));
 echo "Today: {$today}<br>";
 echo "Date: {$date}<br>";
 echo "Difference is {$diff} day(s)";