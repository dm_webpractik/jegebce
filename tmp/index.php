<?php require($_SERVER['DOCUMENT_ROOT']."/admin/init.php");
function getDigitalOrder($str = "200000000", $lang = "en"){
$numbers_rus = array ("до тысячи", "тысячи", "миллионы", "миллиарды", "триллионы", "квадриллионы", "квинтиллионы", "секстиллионы", "септиллионы", "октиллионы", "нониллионы", "дециллионы", "андециллионы", "дуодециллионы", "тредециллионы");
$numbers_eng = array ("0", "thousands", "millions", "billions", "trillions", "quadrillions", "quintillions", "sextillions", "septillions", "oktillions", "nonillions", "decillions", "andecillions", "duodecillions", "tredecillions");
if (isset($_REQUEST["lang"])){if ($_REQUEST["lang"]=="rus") $lang = "ru";}
$numbers = ($lang == "ru") ? $numbers_rus : $numbers_eng;
echo "Source: ".$str."<br>Target: ";
$str = preg_replace("/[^0-9]/","", $str);
//$str = "340,282,366,920,938,463,463,374,607,431,768,211,456";
$strlength = strlen($str);
    if ($strlength<4) return $str;
$order = intval($strlength/3);
$digits = "";
$offset = 0;
$digitlastorder = $strlength - 3*(intval($strlength/3));
$digitlastorder = $digitlastorder ? $digitlastorder : 3;
for ($i=0; $i<$digitlastorder; $i++) {
    $digits .= $str[$i];
}
if ($strlength%3==0) --$order;

$digits .= $numbers[$order];

while ($order-$offset>0){
    for ($i=0; $i<3; $i++) {
        $digits .= $str[$digitlastorder+$offset*3+$i];
    }
    $offset++;
    if ($order-$offset>0) $digits .= $numbers[$order-$offset];
}

    return $digits;
}

if (isset($_POST["da_btn"])) echo getDigitalOrder(trim($_POST["da"]));

if (isset($_POST["submit"])) {
    echo trim($_POST["text"]) . "<br>";
     echo "Length of string is " .strlen($_POST["text"]) . " chars. <br>";
}
if (isset($_POST["hash_btn"])) {
    echo sha1($_POST["hash"]);
}
?>
<div>
<br>
    <span><?echo "\t";?>String length</span>
<br>
<form name="count" method="POST">
<input type="text" name="text" placeholder="Type text here" size="60">
<input type="submit" name="submit" value="Calculate">
<br>
    <span>SHA1 Generator</span>
<br>
<input type="text" name="hash" placeholder="Type text here..." size="40">
<input type="submit" name="hash_btn" value="Generate">
<br>
    <span>Digits analyzer</span>
<br>
<input type="text" name="da" placeholder="Type digit here..." size="60">
<input type="submit" name="da_btn" value="Analyze">
</form>
</div>