$("#go_btn").on('click', function(){
    $(".output").html($("#txt_field").val());
    $(".output").css('color',$("#txt_color").val());
    });

$("#h_input").on('input drag', function () {
    $('.height_value').html($("#h_input").val());
    $(".square").css('height',$("#h_input").val());
});

$("#w_input").on('input change', function () {
    $('.width_value').html($("#w_input").val());
    $(".square").css('width',$("#w_input").val());
});

$("#app_btn").on('click', function(){
    $(".square").css('height',$("#h_input").val());
    $(".square").css('width',$("#w_input").val());
    $(".square").css('color',$("#col_input").val());
    $(".square").css('background',$("#bg_input").val());
    $(".square").css('border-color',$("#brd_input").val());
    $(".square h1").html($("#txt_input").val());
});

$("#rst_btn").on('click', function(){
    $('.height_value').html("100");
    $('.width_value').html("100");
    $(".square").css('height','100');
    $(".square").css('width','100');
    $(".square").css('color','#990000');
    $(".square").css('background','#cccccc');
    $(".square").css('border-color','#000099');
    $(".square h1").html("TEXT");
});

$(".settings").on('click', function () {
    if ($(".spec").is(":hidden")) {
        $(".middle").animate({
            width:"500px"

        },1500);
        $(".spec").show(1000);
    }
    else {
        $(".middle").animate({
            width:"250px"
        },1000);
        $(".spec").hide(1000);
    }
});
