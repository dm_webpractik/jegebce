<?php
function download($filename) {
    if (file_exists($filename)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Disposition: attachment; filename=" . basename($filename) . ";"); // Указываем имя при сохранении в браузере
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . strlen($filename));
        echo file_get_contents($filename); // Отдаём файл пользователю на скачивание
    }
    else echo "Not Found"; // Если файла не существует
}
$path = $_SERVER["DOCUMENT_ROOT"].'/upload/prices/price.xlsx';
//var_dump($path);
download($path);
