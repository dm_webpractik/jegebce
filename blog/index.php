<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/header.php');?>
<div class="topper"></div>
<div class="top_header">
    <h1>Blog</h1>
</div>
    <div class="content">
        <article>
            <div class="article_content">
                <img src="/img/noPic.png" alt="">
                <div class="right_side">
                    <div class="date">22-June-20019</div><h4 class="article-title-h4">Title of article</h4>
                    <p class="main_paragraph">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam atque eveniet facilis fuga fugiat maxime minus molestiae voluptatibus! Aperiam illo minus numquam repellat rerum sunt vel! Aliquam id incidunt tempora?
                    </p>
                </div>
            </div>
        </article>
    </div>
<?php include_once ($_SERVER['DOCUMENT_ROOT'].'/footer.php');?>
