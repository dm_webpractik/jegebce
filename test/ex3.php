<?php
/**
 * Created by PhpStorm.
 * User: DM
 * Date: 25.05.2019
 * Time: 13:57
 */
function imgTag($url, $alt="default", $height = 50, $width = 50) {
    $path = $GLOBALS["IMG_PATH"];
    $string = "<img src='{$path}{$url}' alt='{$alt}' height='{$height}' width='{$width}'>";
    return $string;
}

function hexColor($red, $green, $blue){
    $string = "#";
    $string .= str_pad(dechex($red), 2, '0', STR_PAD_LEFT);
    $string .= str_pad(dechex($green), 2, '0', STR_PAD_LEFT);
    $string .= str_pad(dechex($blue), 2, '0', STR_PAD_LEFT);
    return $string;
}

$population = array("California" => array("Los Angeles" => 3792621,
                                            "San Diego" => 1307402,
                                            "San Hose" => 945942),
                    "Illinois" => array("Chicago" => 2695598),
                    "Texas" => array("Huston" => 2100263,
                                    "San Antonio" => 1327407,
                                    "Dallas" => 1197816),
                    "Arizona" => array("Phoenix" => 1445632),
                    "New York" => array("New York" => 8175133));
asort($population);
$row_type = array("even", "odd");
$style_index = 0; $grand_total = 0;
echo "<table border='1'>";
foreach ($population as $state => $cities) {
    $total=0;
    foreach ($cities as $city => $inhabitants) {
        echo "<tr class='{$row_type[$style_index]}'>";
        echo "<td> $city - $inhabitants</td></tr>";
        $total += $inhabitants;
        $style_index = 1 - $style_index;
    }
    echo "<tr class='state'><td>$state: $total</td></tr>";
    $grand_total += $total;
}
echo "<tr class='total'><td>Overall population: $grand_total</td></tr>";
echo "</table>";
echo "<pre>";
$GLOBALS["IMG_PATH"] = '../img/';
echo "Your IP is ".$_SERVER["GEOIP_ADDR"]."<br>";
echo "Your city is ".$_SERVER["GEOIP_CITY"]."<br>";
echo imgTag("logo.png", "logo", 50, 200);

$bg_color = hexColor(100, 120, 130);
?>
<style>
    .odd {
        background-color: #208182;
    }
    .state {
        background-color: #f0ef42;
    }
    .total {
        background-color: #4867f6;
    }
    .test {
        display: block;
        width: 200px;
        height: 200px;
    }
</style>
<div><?=$bg_color?></div>
<div class="test" style="background-color: <?=$bg_color?>"></div>
