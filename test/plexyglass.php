<?php
/**
 * Created by PhpStorm.
 * User: DM
 * Date: 21.12.2019
 * Time: 11:27
 */?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<style>
    html, body, div, span, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, abbr, address, cite, code, del, dfn, em, img, ins, kbd, q, samp, small, strong, sub, sup, var, b, i, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, figcaption, figure, footer, header, hgroup, menu, nav, section, summary, time, mark, audio, video {
        margin: 0;
        padding: 0;
        border: 0;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background: transparent;
    }
    .outer-container {
        background-color: #fff;
        box-shadow: 0 0 7px 0 rgba(0, 0, 0, 0.35);
        position: relative;
        max-width: 1100px;
        margin: 0 auto;
        z-index: 1;
    }
    .headline {
        background: rgba(200, 240, 240, 0.6);
    }
    ::-moz-focus-inner {
        border: none;
        margin: 0;
        padding: 0;
    }
    .layout-boxed .outer-container, .container {
        max-width: 1100px;
    }
    .layout-boxed .outer-container {
        max-width: 980px;
    }
    .outer-container.transparent {
        background: none;
    }
    .headline .section {
        padding-top: 14px;
        padding-bottom: 14px;
        overflow: hidden;
    }
    .mobile {
        display: none;
    }
    .movable-container ul {
        list-style: none;
        display: flex;
        justify-content: space-evenly;
    }
</style>
<div class="backgrounds"><div style="background: url('//leonov-do.ru/wp-content/uploads/2015/07/239_leather01.png') #ffffff repeat left top fixed; background-size: 300px 300px;"></div></div>
<div class="upper-container ">

    <div class="outer-container">

        <nav id="menu" class="mobile">
            <ul id="menu-menyu1" class=""><li id="menu-item-1849" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1849"><a href="//leonov-do.ru/"><i class="icon-dot"></i>Главная</a></li><li id="menu-item-1851" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1851"><a href="//leonov-do.ru/karta-sajta"><i class="icon-dot"></i>Все статьи блога</a></li><li id="menu-item-1853" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1853"><a href="//leonov-do.ru/ob-avtore"><i class="icon-dot"></i>Об авторе</a></li><li id="menu-item-1852" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1852"><a href="//leonov-do.ru/kontakty"><i class="icon-dot"></i>Обратная связь</a></li><li id="menu-item-1850" class="menu-item menu-item-type-post_type menu-item-object-page current current-menu-item page_item page-item-995 current current_page_item menu-item-1850"><a href="//leonov-do.ru/uslugi" aria-current="page"><i class="icon-dot"></i>Услуги</a></li><li id="menu-item-2226" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2226"><a href="//leonov-do.ru/rekomenduyu"><i class="icon-dot"></i>Рекомендую</a></li></ul>						</nav>

        <nav id="search" class="mobile">
            <form method="get" action="//leonov-do.ru/" class="search" role="search"><input type="text" name="s" value="" placeholder="Поиск на сайте"><button type="submit"><i class="icon-search"></i></button></form>						</nav>

    </div>

    <div class="outer-container ">

        <header class="header">

            <div class="container">

                <div class="mobile-helper vertical-align" style="top: 52.5px;">

                    <a href="#menu" class="button" title="Меню"><i class="icon-menu"></i></a>


                    <a href="#search" class="button" title="Поиск"><i class="icon-search"></i></a>

                </div>


                <h1 class="logo vertical-align center" style="top: 0px;"><a href="//leonov-do.ru/" title="Блог Леонова Дмитрия" rel="home"><img src="//leonov-do.ru/wp-content/uploads/2015/02/Header1100_150.png" alt="Блог Леонова Дмитрия" width="1100" height="150"></a></h1>

            </div>

        </header>


        <nav class="secondary">
            <div class="container">
                <div class="movable-container" style="margin-top: 0px; margin-bottom: 0px;"><ul id="menu-menyu1-1" class="movable-container-content" style="left: 0px;"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1849"><a href="//leonov-do.ru/">Главная</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1851"><a href="//leonov-do.ru/karta-sajta">Все статьи блога</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1853"><a href="//leonov-do.ru/ob-avtore">Об авторе</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1852"><a href="//leonov-do.ru/kontakty">Обратная связь</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page current current-menu-item page_item page-item-995 current current_page_item menu-item-1850"><a href="//leonov-do.ru/uslugi" aria-current="page">Услуги</a></li><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2226"><a href="//leonov-do.ru/rekomenduyu">Рекомендую</a></li></ul></div>		</div>
        </nav>

    </div>

</div>
<div class="outer-container transparent">

    <div class="headline">

        <div class="container">

            <div class="section">
                <h1 class="entry-title">Услуги</h1>
            </div>

        </div>

    </div>

</div>
<div class="outer-container" style="height: auto !important;">



    <div class="content" style="height: auto !important;"><div class="container" style="height: auto !important;"><div class="main alpha" style="padding: 0px 350px 0px 0px; margin: 0px -350px 0px 0px; height: auto !important;">

                <section class="section">
                    <article id="post-995" class="post hentry post-995 page type-page status-publish">


                        <p>Уважаемый&nbsp;посетитель, не секрет что интернет развивается стремительно, и наличие своего сайта уже не является чем-то необычным. Приведу несколько доводов для того что-бы завести свой сайт:</p>
                        <ul>
                            <li>Вы хотите зарабатывать в интернете;</li>
                            <li>Вы хотите вести&nbsp;свой блог;</li>
                            <li>Вам нужна реклама&nbsp;своего бизнеса в интернете;</li>
                            <li>Вы заинтересованы в увеличении количество клиентов, прибыли и продаж;</li>
                            <li>Вы хотите освещать своего хобби.</li>
                        </ul>
                        <h3>
                            <figure id="attachment_1001" class="wp-image-1001 size-thumbnail alignright fixed" style="width: 150px;">
                                <div class="inset-border"><img data-2x="//leonov-do.ru/wp-content/uploads/2014/11/Uslugi.jpg" src="//leonov-do.ru/wp-content/uploads/2014/11/Uslugi-150x150.jpg" alt="Услуги" width="150" height="150"></div>
                                <p></p></figure><p></p>
                            Я создам Вам&nbsp;сайт&nbsp;на WordPress под ключ со стандартной темой</h3>
                        <p>От Вас потребуется выбрать шаблон блога (существует множество бесплатных тем), и подробно описать его структуру. В течение нескольких дней Ваш блог будет готов к использованию. Стоимость работы от <span style="color: #993300;"><strong>2500 рублей</strong></span>, не включая покупку хостинга и домена (примерная стоимость 1000—1500 руб. в год)</p>
                        <h3>&nbsp;В итоге вы получите:</h3>
                        <ol>
                            <li>Создание основных страниц (Главная, О сайте, Обо мне или О Компании, Контакты);</li>
                            <li>Установка системы WordPress на хостинге;</li>
                            <li>Установка шаблона (темы);</li>
                            <li>Общая настройка сайта;</li>
                            <li>Настройка виджетов;</li>
                            <li>Базовый пакет плагинов;</li>
                            <li>Регистрация и настройка Feedburner (подписка на обновления сайта);</li>
                            <li>Регистрация в сервисе рассылки&nbsp;(создание собственной рассылки);</li>
                            <li>Доменное имя (пример mysait.ru);</li>
                            <li>Хостинг (сервер, где хранятся все файлы Вашего сайта).&nbsp;</li>
                        </ol>
                        <p>По всем вопросам Вы можете связаться со мной заполнив форму на странице<em> <a title="Обратная связь" href="//leonov-do.ru/kontakty" target="_blank">Обратная связь.</a></em></p>
                        <p>&nbsp;</p>
                        <script type="text/javascript">(function(w,doc) {
                                if (!w.__utlWdgt ) {
                                    w.__utlWdgt = true;
                                    var d = doc, s = d.createElement('script'), g = 'getElementsByTagName';
                                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                                    s.src = ('' == w.location.protocol ? 'https' : 'http')  + '://w.uptolike.com/widgets/v1/uptolike.js';
                                    var h=d[g]('body')[0];
                                    h.appendChild(s);
                                }})(window,document);
                        </script>
                        <div style="text-align:center;" data-lang="ru" data-url="//leonov-do.ru/uslugi" data-background-alpha="0.0" data-buttons-color="#ff9300" data-counter-background-color="#ffffff" data-share-counter-size="14" data-top-button="false" data-share-counter-type="common" data-share-style="2" data-mode="share" data-like-text-enable="false" data-hover-effect="scale" data-mobile-view="true" data-icon-color="#ffffff" data-orientation="horizontal" data-text-color="#000000" data-share-shape="rectangle" data-sn-ids="fb.tw.ok.vk.gp." data-share-size="40" data-background-color="#ededed" data-preview-mobile="false" data-mobile-sn-ids="fb.vk.tw.wh.ok.vb." data-pid="cmsleonovdoru" data-counter-background-alpha="1.0" data-following-enable="false" data-exclude-show-more="false" data-selection-enable="true" class="uptolike-buttons"></div>
                    </article>
                </section>







                <section id="comments" class="section">
                    <ul class="comments">

                        <li id="comment-3424">
                            <section class="comment">
                                <figure class="alignleft fixed inset-border">
                                    <img alt="" src="//secure.gravatar.com/avatar/ba5cf12b318cdd68d456dd40e9713e6e?s=50&amp;r=g" srcset="//secure.gravatar.com/avatar/ba5cf12b318cdd68d456dd40e9713e6e?s=100&amp;r=g 2x" class="avatar avatar-50 photo" width="50" height="50">			</figure>
                                <p class="info">
                                    <small class="tools alt">
                                        <a rel="nofollow" class="comment-reply-link" href="#comment-3424" data-commentid="3424" data-postid="995" data-belowelement="comment-3424" data-respondelement="respond" aria-label="Комментарий к записи Николай">ответить</a>							</small>
                                    <strong>Николай</strong>
                                    , <time class="small" datetime="2017-03-26T20:43Z">
                                        3 года назад				</time>
                                </p>
                                <article class="text">
                                    <p>Здравствуйте Дмитрий! Посмотрел ваше видео «Как сделать копию лендинга (одностраничника).» Как раз на данном этапе для меня интересна эта тема. В общем то всё просто,  да не всё. Второй способ мне понравился больше, но не получилось установить прогу на Windows xp. А в случае со SkrapBookom не понятно как менять форму отправки заказа? Буду признателен если подскажете...</p>		</article>
                            </section>
                            <ul class="comments">
                                <li id="comment-3425">
                                    <section class="comment">
                                        <figure class="alignleft fixed inset-border">
                                            <img alt="" src="//secure.gravatar.com/avatar/a3db4e0ccf86c2646e625ccb439529f5?s=50&amp;r=g" srcset="//secure.gravatar.com/avatar/a3db4e0ccf86c2646e625ccb439529f5?s=100&amp;r=g 2x" class="avatar avatar-50 photo" width="50" height="50">			</figure>
                                        <p class="info">
                                            <small class="tools alt">
                                                <a rel="nofollow" class="comment-reply-link" href="#comment-3425" data-commentid="3425" data-postid="995" data-belowelement="comment-3425" data-respondelement="respond" aria-label="Комментарий к записи Леонов Дмитрий">ответить</a>							</small>
                                            <strong>Леонов Дмитрий</strong>
                                            , <time class="small" datetime="2017-03-26T21:08Z">
                                                3 года назад				</time>
                                        </p>
                                        <article class="text">
                                            <p>Добрый день Николай. С формой заказа не так все просто. Это отдельный код. У каждой системы своя форма заказа. Наиболее простой вариант вообще убирать с лендинга форму заказа, оставить только кнопку, по нажатию которой происходит переход по ссылке, вызывается форма заказа (подписки). Как правило у всех систем в личном кабинете есть такая ссылка.</p>		</article>
                                    </section>
                                    <ul class="comments"></ul></li></ul></li>
                        <li id="comment-7963">
                            <section class="comment">
                                <figure class="alignleft fixed inset-border">
                                    <img alt="" src="//secure.gravatar.com/avatar/a48dc77f80289c229bbd1910a98d76d0?s=50&amp;r=g" srcset="//secure.gravatar.com/avatar/a48dc77f80289c229bbd1910a98d76d0?s=100&amp;r=g 2x" class="avatar avatar-50 photo" width="50" height="50">			</figure>
                                <p class="info">
                                    <small class="tools alt">
                                        <a rel="nofollow" class="comment-reply-link" href="#comment-7963" data-commentid="7963" data-postid="995" data-belowelement="comment-7963" data-respondelement="respond" aria-label="Комментарий к записи Владимир">ответить</a>							</small>
                                    <strong>Владимир</strong>
                                    , <time class="small" datetime="2019-04-05T15:37Z">
                                        9 месяцев назад				</time>
                                </p>
                                <article class="text">
                                    <p>Дмитрий, а как на счет сложных проектов? Например был заказ сделать сайт функционалом как «Юла». Я сам такие проекты не делаю, сложно. Вы такое умеете?</p>		</article>
                            </section>
                            <ul class="comments">
                                <li id="comment-7966">
                                    <section class="comment">
                                        <figure class="alignleft fixed inset-border">
                                            <img alt="" src="//secure.gravatar.com/avatar/a3db4e0ccf86c2646e625ccb439529f5?s=50&amp;r=g" srcset="//secure.gravatar.com/avatar/a3db4e0ccf86c2646e625ccb439529f5?s=100&amp;r=g 2x" class="avatar avatar-50 photo" width="50" height="50">			</figure>
                                        <p class="info">
                                            <small class="tools alt">
                                                <a rel="nofollow" class="comment-reply-link" href="#comment-7966" data-commentid="7966" data-postid="995" data-belowelement="comment-7966" data-respondelement="respond" aria-label="Комментарий к записи Леонов Дмитрий">ответить</a>							</small>
                                            <strong>Леонов Дмитрий</strong>
                                            , <time class="small" datetime="2019-04-05T19:35Z">
                                                9 месяцев назад				</time>
                                        </p>
                                        <article class="text">
                                            <p>Владимир, Юла как и Авито это сложные проекты, разработка может стоить очень дорого. Это для профессионалов</p>		</article>
                                    </section>
                                    <ul class="comments"></ul></li></ul></li>			</ul>
                </section>


                <section class="section">
                    <div id="respond" class="comment-respond">
                        <h3 id="reply-title" class="comment-reply-title">Оставить комментарий <small><a rel="nofollow" id="cancel-comment-reply-link" href="/uslugi#respond" style="display:none;">Отменить отзыв</a></small></h3><form action="//leonov-do.ru/wp-comments-post.php" method="post" id="commentform" class="comment-form"><input type="hidden" autocomplete="off" autocorrect="off" name="P-K-srs-BN-R-dI-A" value="_o-6tyip2diNxgcMQO3hR0X0Nuv7XOW2e7VwQIKD-5PTCXO5otfITjMEdBHM22-w380Mg8jImCmQqSDTceWPGWBmYxodMNOcKGRlfmOud6b-n8HVSBHitCp53aVtD8Uo"><p><textarea class="full-width" name="comment" placeholder="Сообщение"></textarea></p><div class="columns alt-mobile"><ul><li class="col-1-3"><input class="full-width" type="text" name="author" placeholder="Имя*" value=""></li>
                                    <li class="col-1-3 mobile-2-clear-row"><input class="full-width" type="text" name="email" placeholder="E-mail (не будет опубликован)*" value=""></li>
                                    <li class="col-1-3 mobile-1-clear-row mobile-2-clear-row"><input class="full-width" type="text" name="url" placeholder="Вебсайт" value=""></li>
                                </ul></div><p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Отправить ›"> <input type="hidden" name="comment_post_ID" value="995" id="comment_post_ID">
                                <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                            </p><input name="rN-DZp-e-Nf-P-O-C-q-Q" type="hidden" value="13667685.0true"></form>	</div><!-- #respond -->
                </section>



                <div style="width: 100%; height: auto; clear: none; text-align: center;" class="google-auto-placed"><ins style="display: block; margin: 10px auto; background-color: transparent; height: 280px;" data-ad-format="auto" class="adsbygoogle adsbygoogle-noablate" data-ad-client="ca-pub-5435538281030811" data-adsbygoogle-status="done"><ins id="aswift_2_expand" style="display:inline-table;border:none;height:280px;margin:0;padding:0;position:relative;visibility:visible;width:750px;background-color:transparent;"><ins id="aswift_2_anchor" style="display:block;border:none;height:280px;margin:0;padding:0;position:relative;visibility:visible;width:750px;background-color:transparent;"><iframe marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_2" name="aswift_2" style="left: 0px; position: absolute; top: 0px; border: 0px none; width: 750px; height: 280px; overflow: visible;" width="750" height="280" frameborder="0"></iframe></ins></ins></ins></div></div><aside class="aside beta" style="width: 350px; height: auto !important; min-height: 0px !important;"><section id="time-unwrapped-text-4" class="section widget widget-unwrapped-text"><h2 class="title">Добро пожаловать!</h2><p><a href="//leonov-do.ru/ob-avtore"><img class="aligncenter wp-image-1439" title="Дмитрий Леонов" src="//leonov-do.ru/wp-content/uploads/2014/09/DOL_V-282x300.png" alt="Дмитрий Леонов" width="250"></a></p>
                    <p><span style="font-size: 10pt;">Рад знакомству! Меня зовут Дмитрий Леонов и я автор данного блога. <a title="Об авторе" href="//leonov-do.ru/ob-avtore">Немного обо мне...</a></span></p></section><section id="time-unwrapped-text-5" class="section widget widget-unwrapped-text"><h2 class="title">Следуй за мной!</h2><a href="//www.youtube.com/channel/UCKCn8ou2ShzXXSOksl7HHLQ" target="_blank" rel="nofollow"><img alt="YouTube" title="YouTube" src="//leonov-do.ru/wp-content/uploads/2015/01/youtube.png" width="44" height="44"></a>
                    <a href="//www.facebook.com/dmitriy.leonov.35" target="_blank" rel="nofollow"><img alt="Facebook" title="Facebook" src="//leonov-do.ru/wp-content/uploads/2015/01/facebook.png" width="44" height="44"></a>
                    <a href="//vk.com/dol72" target="_blank" rel="nofollow"><img alt="В Контакте" title="В Контакте" src="//leonov-do.ru/wp-content/uploads/2015/01/vkontakte.png" width="44" height="44"></a>
                    <a href="//twitter.com/DOL_Krasnodar" target="_blank" rel="nofollow"><img alt="Twitter" title="Twitter" src="//leonov-do.ru/wp-content/uploads/2015/01/twitter.png" width="44" height="44"></a>
                    <a href="//plus.google.com/u/0/+Leonov-doRu/posts" target="_blank" rel="nofollow"><img alt="Google+" title="Google+" src="//leonov-do.ru/wp-content/uploads/2015/01/google.png" width="44" height="44"></a></section><section id="time-unwrapped-text-6" class="section widget widget-unwrapped-text" style="height: auto !important;"><h2 class="title">Подарок от автора</h2><noindex><div style="text-align:center;">
                            <a href="//leonov-do.ru/wppage/camtasia-studio" target="_blank" rel="nofollow">
                                <img style="border: 0px solid; " src="//leonov-do.ru/wp-content/uploads/2015/06/camtasia400.png" width="340"></a></div><div style="width: 100%; height: auto; clear: none; text-align: center;" class="google-auto-placed"><ins style="display: block; margin: 10px auto; background-color: transparent; height: 250px;" data-ad-format="auto" class="adsbygoogle adsbygoogle-noablate" data-ad-client="ca-pub-5435538281030811" data-adsbygoogle-status="done"><ins id="aswift_1_expand" style="display:inline-table;border:none;height:250px;margin:0;padding:0;position:relative;visibility:visible;width:310px;background-color:transparent;"><ins id="aswift_1_anchor" style="display:block;border:none;height:250px;margin:0;padding:0;position:relative;visibility:visible;width:310px;background-color:transparent;"><iframe marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_1" name="aswift_1" style="left: 0px; position: absolute; top: 0px; border: 0px none; width: 310px; height: 250px; overflow: visible;" width="310" height="250" frameborder="0"></iframe></ins></ins></ins></div></noindex></section><section id="categories-2" class="section widget widget_categories"><h2 class="title">Каталог статей</h2>		<ul class="fancy alt">
                        <li class="cat-item cat-item-145"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/youtube" title="В этом разделе Вы найдете статьи посвященные YouTube. Полезная информация поможет Вам разобраться со всеми тонкостями самого популярного видеохостинга в мире.">YouTube</a> (3)
                        </li>
                        <li class="cat-item cat-item-3"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/dosug">Досуг</a> (4)
                        </li>
                        <li class="cat-item cat-item-115"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/zarabotok-v-internete">Заработок</a> (7)
                        </li>
                        <li class="cat-item cat-item-12"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/info-material">Инфо-материал</a> (3)
                        </li>
                        <li class="cat-item cat-item-4"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/infobiznes">Инфобизнес</a> (10)
                        </li>
                        <li class="cat-item cat-item-1"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/novosti">Новости</a> (9)
                        </li>
                        <li class="cat-item cat-item-58"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/optimizaciya-sajta">Оптимизация</a> (19)
                        </li>
                        <li class="cat-item cat-item-18"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/saitostroenie">Сайтостроение</a> (35)
                            <ul class="children fancy alt">
                                <li class="cat-item cat-item-98"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/saitostroenie/dizajn-saitostroenie">Дизайн</a> (3)
                                </li>
                                <li class="cat-item cat-item-69"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/saitostroenie/plaginy">Плагины</a> (11)
                                </li>
                            </ul>
                        </li>
                        <li class="cat-item cat-item-62"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/programmi">Софт, IT</a> (9)
                        </li>
                        <li class="cat-item cat-item-107"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/socialnye-seti">Соцсети</a> (5)
                        </li>
                        <li class="cat-item cat-item-88"><i class="icon-right-open color"></i><a href="//leonov-do.ru/category/uchitelskie-bajki">Учительские байки</a> (2)
                        </li>
                    </ul>
                </section><section id="time-unwrapped-text-25" class="section widget widget-unwrapped-text"><h2 class="title">Курс по WordPress</h2><noindex><div style="text-align:center;">
                            <a href="//leonov-do.ru/wp-basa" ;="" target="_blank" rel="nofollow">
                                <img style="border: 0px solid; " src="//leonov-do.ru/wp-content/uploads/2019/06/popov-wp-basa.jpg"></a></div></noindex></section><section id="time-unwrapped-text-16" class="section widget widget-unwrapped-text" style="height: auto !important;"><h2 class="title">Руководство пользователя</h2><noindex><div style="text-align:center;">
                            <a href="//leonov-do.ru/atp" ;="" target="_blank" rel="nofollow">
                                <img style="border: 0px solid; " src="//leonov-do.ru/wp-content/uploads/2017/08/250_400-для-сайта.jpg" ;="" width="250"></a></div><div style="width: 100%; height: auto; clear: none; text-align: center;" class="google-auto-placed"><ins style="display: block; margin: 10px auto; background-color: transparent; height: 250px;" data-ad-format="auto" class="adsbygoogle adsbygoogle-noablate" data-ad-client="ca-pub-5435538281030811" data-adsbygoogle-status="done"><ins id="aswift_3_expand" style="display:inline-table;border:none;height:250px;margin:0;padding:0;position:relative;visibility:visible;width:310px;background-color:transparent;"><ins id="aswift_3_anchor" style="display:block;border:none;height:250px;margin:0;padding:0;position:relative;visibility:visible;width:310px;background-color:transparent;"><iframe marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i=this.id,s=window.google_iframe_oncopy,H=s&amp;&amp;s.handlers,h=H&amp;&amp;H[i],w=this.contentWindow,d;try{d=w.document}catch(e){}if(h&amp;&amp;d&amp;&amp;(!d.body||!d.body.firstChild)){if(h.call){setTimeout(h,0)}else if(h.match){try{h=s.upd(h,i)}catch(e){}w.location.replace(h)}}" id="aswift_3" name="aswift_3" style="left: 0px; position: absolute; top: 0px; border: 0px none; width: 310px; height: 250px; overflow: visible;" width="310" height="250" frameborder="0"></iframe></ins></ins></ins></div></noindex></section><section id="time-unwrapped-text-26" class="section widget widget-unwrapped-text"><h2 class="title">Мощный плагин для WordPres</h2><script src="//wpwidget.ru/js/wps-widget-entry.min.js" async=""></script>
                    <div class="wps-widget wps-widget-created" data-w="//wpwidget.ru/greetings?orientation=2&amp;pid=1410" style="height: 410px;"><iframe id="wpshop_widget_AcVuMDq15vmTqggQ6VvuC0e7dzMuvfJC" src="//wpwidget.ru/greetings?orientation=2&amp;pid=1410&amp;uid=AcVuMDq15vmTqggQ6VvuC0e7dzMuvfJC&amp;host=leonov-do.ru&amp;width=310" width="100%" height="100%" frameborder="0"></iframe></div></section><section id="search-2" class="section widget widget_search"><form method="get" action="//leonov-do.ru/" class="search" role="search"><input type="text" name="s" value="" placeholder="Поиск на сайте"><button type="submit"><i class="icon-search"></i></button></form></section></aside></div></div>
</div>
</body>
</html>
