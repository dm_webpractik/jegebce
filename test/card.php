<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Оплата через AzeriCard</title>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= LANG_CHARSET ?>">
</head>
<body bgColor="#ffffff">
<p><font class="tablebodytext"><b>Заказ № 123654 от <?= date("d.m.Y H:i:s", time())?></b></font></p>
<p>Сумма к оплате: <b>56.14 RUR</b></p>
<form ACTION="http://viplife2.x5test.ru/personal/order/payment/process.php" METHOD="POST">
    <?php
    // Getting required fields

    // These fields can change in every request
    $db_row['AMOUNT'] = 56.14;
    $db_row['CURRENCY'] = "RUR";
    $db_row['ORDER'] = 123654;

    // These fields will be always static
    $db_row['DESC'] = 'Goods sale';
    $db_row['MERCH_NAME'] = 'VipLife';
    $db_row['MERCH_URL'] = 'http://viplife2.x5test.ru/personal/order/payment/success.php';
    $db_row['TERMINAL'] = '17201238';			// That is your personal ID in payment system
    $db_row['EMAIL'] = 'deemmoor@yandex.ru';
    $db_row['TRTYPE'] = '1';					// That is the type of operation, 1 - Authorization and checkout
    $db_row['COUNTRY'] = 'AZ';
    $db_row['MERCH_GMT'] = '+4';
    $db_row['BACKREF'] = 'http://viplife2.x5test.ru/personal/order/payment/process.php';

    //These fields are generated automaticaly every request
    $oper_time=gmdate("YmdHis");			// Date and time UTC
    $nonce=substr(md5(rand()),0,16);		// Random data

    // ------------------------------
    // Creating form hidden fields

    echo "
	<input name=\"AMOUNT\" value=\"{$db_row['AMOUNT']}\" type=\"hidden\">
    <input name=\"CURRENCY\" value=\"{$db_row['CURRENCY']}\" type=\"hidden\">
	<input name=\"ORDER\" value=\"{$db_row['ORDER']}\" type=\"hidden\">
	<input name=\"DESC\" value=\"{$db_row['DESC']}\" type=\"hidden\">
    <input name=\"MERCH_NAME\" value=\"{$db_row['MERCH_NAME']}\" type=\"hidden\">
    <input name=\"MERCH_URL\" value=\"{$db_row['MERCH_URL']}\" type=\"hidden\">
    <input name=\"TERMINAL\" value=\"{$db_row['TERMINAL']}\" type=\"hidden\">
    <input name=\"EMAIL\" value=\"{$db_row['EMAIL']}\" type=\"hidden\">
    <input name=\"TRTYPE\" value=\"{$db_row['TRTYPE']}\" type=\"hidden\">    
    <input name=\"COUNTRY\" value=\"{$db_row['COUNTRY']}\" type=\"hidden\"> 
	<input name=\"MERCH_GMT\" value=\"{$db_row['MERCH_GMT']}\" type=\"hidden\"> 
	<input name=\"TIMESTAMP\" value=\"{$oper_time}\" type=\"hidden\">
	<input name=\"NONCE\" value=\"{$nonce}\" type=\"hidden\">
	<input name=\"BACKREF\" value=\"{$db_row['BACKREF']}\" type=\"hidden\">
	<input name=\"LANG\" value=\"AZ\" type=\"hidden\">
";

    // ------------------------------------------------
    // Making P_SIGN (MAC)	-         Checksum of request
    // All following fields must be equal with hidden fields above

    $to_sign = "".strlen($db_row['AMOUNT']).$db_row['AMOUNT']
        .strlen($db_row['CURRENCY']).$db_row['CURRENCY']
        .strlen($db_row['ORDER']).$db_row['ORDER']
        .strlen($db_row['DESC']).$db_row['DESC']
        .strlen($db_row['MERCH_NAME']).$db_row['MERCH_NAME']
        .strlen($db_row['MERCH_URL']).$db_row['MERCH_URL']
        .strlen($db_row['TERMINAL']).$db_row['TERMINAL']
        .strlen($db_row['EMAIL']).$db_row['EMAIL']
        .strlen($db_row['TRTYPE']).$db_row['TRTYPE']
        .strlen($db_row['COUNTRY']).$db_row['COUNTRY']
        .strlen($db_row['MERCH_GMT']).$db_row['MERCH_GMT']
        .strlen($oper_time).$oper_time
        .strlen($nonce).$nonce
        .strlen($db_row['BACKREF']).$db_row['BACKREF'];

    $key_for_sign="fc73fa1518aee9f456652abbad97e57d"; 				// Key for sign will change in production system
    $p_sign=hash_hmac('sha1',$to_sign, hex2bin($key_for_sign));

    echo "<input name=\"P_SIGN\" value=\"$p_sign\" type=\"hidden\">";
    // ----------------------------------------------------

    ?>
    <input alt="Submit" type="submit" value="Оплатить" style="margin-bottom: 50px">
</form>
</body>
</html>
