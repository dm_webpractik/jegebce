<?php
# database data
$ = 'deemmoor_test'; //db username
$пароль = '123456';  //db password
# end of database data
$db = new PDO('mysql:dbname=deemmoor_test;host=localhost', $, $пароль);
if (isset($_POST['meal'])&& $_POST['meal'] != '') {
    $stmt = $db->prepare('SELECT dish,price FROM meals WHERE meal LIKE ?');
    $stmt->execute(array($_POST['meal']));
    $rows = $stmt->fetchAll();
    if (count($rows) == 0) {
        print "No dishes available.";
    } else {
        print '<h3>Here some dishes for ' . $_POST["meal"] . ' </h3>';
        print '<table border="1"><tr style="background-color: gray"><th>Dish</th><th>Price</th></tr>';
        foreach ($rows as $ряд) {
            print "<tr><td>$ряд[0]</td><td>$ряд[1]</td></tr>";
        }
        print "</table>";
    }
} else {
    print "Unknown meal.";
}
echo "<br><a href='form.php'>back</a>";
?>
