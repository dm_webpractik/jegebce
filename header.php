<?php require($_SERVER['DOCUMENT_ROOT']."/admin/init.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="yandex-verification" content="f117984c832c606d" />
    <meta name="yandex-verification" content="fe71260d4ae3cd7f" />
    <meta name="yandex-verification" content="2a5e9ac79d4c5225" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="index, follow">
    <meta name="title" content="JEGEBCE | Anthology of Rock" />
    <meta name="keywords" content="jegebce, history of rock group jegebce, Deem Moor, Andrew Cross" />
    <meta name="description" content="Official Website of rock group Jegebce. Photo, Video and Audio materials that had created by Deem Moor and Andrew Cross. History from 1993 till nowadays." />
    <meta name="geo.region" content="US-TN" />
    <meta name="geo.placename" content="Nashville, USA" />
    <meta name="geo.position" content="36.162664;-86.781602" />
    <meta name="ICBM" content="36.162664, -86.781602" />
    <title>JEGEBCE | Anthology</title>
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="icon" href="/ico/favicon.png" type="image/x-icon">
    <script src="https://www.google.com/recaptcha/api.js?render=6LcZpMAUAAAAAEKGsW54zpyhxQeHcLGJd-5kurlZ"></script>
</head>
<body>
<div class="main">
<header id="header">
    <div class="topline">
        <div class="container">
            <div class="wrap">
                 <div class="logo">
                    <a href="/">jegebce.online</a>
                </div>
                <div class="socials">
                    <?require_once ($_SERVER['DOCUMENT_ROOT']."/include/socials.php");?>
                </div>
                <div class="search">
                    <form name="search" action="/search/">
                        <input name="searchtext" type="search" placeholder="Search:" value="">
                        <button name="searchbutton" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </form>
                </div>
                <div class="auth">
                    <?if (isset($_COOKIE["id"])&&(!empty($_COOKIE["id"]))) {
                        session_start();
                        $_SESSION["ID"] = $_COOKIE["id"];
                        ?>
                        <i class="fas fa-user"></i>
                        <span><?echo GetUser::getNameById($_COOKIE['id']);?> [<a href="/personal/index.php"><?echo GetUser::getLoginById($_COOKIE['id']);?></a>]</span>
                        <span>|</span>
                        <i class="fas fa-sign-out-alt"></i>
                        <a href="/auth/logout.php"><span>logout</span></a>
                    <?} else {
                    ?>
                    <i class="fas fa-sign-in-alt"></i>
                        <a class="auth_letter" href="/auth" onclick="changeColor(this);">Login</a>
                        <span>|</span>
                        <a class="auth_letter" href="/auth/register.php" onclick="changeColor(this);">Register</a>
                    <?}?>
                </div>
                <div class="clock">
                    <iframe width="120" height="30" src="https://yandex.ru/time/widget/?geoid=39&type=digital"></iframe>
                </div>
            </div>
            <nav>
                <div class="topmenu">
                    <div class="menuBurger">
                        <span class="bg_lines"><i class="fas fa-bars"></i></span>
                        <div class="auth_mobile">
                            <?if (isset($_COOKIE["id"])&&(!empty($_COOKIE["id"]))) {
                                ?>
                                <a href="/personal/index.php">
                                <i class="fas fa-user"></i>
                                <span><?echo GetUser::getNameById($_COOKIE['id']);?></span>
                                </a>
                            <?} else {
                                ?>
                                <a href="/auth">
                                <i class="fas fa-sign-in-alt"></i>
                                </a>
                            <?}?>
                        </div>
                    </div>
                    <ul>
                        <?php
                        $curDir = substr(getcwd(), strrpos(getcwd(), "/"));
                        foreach ($arMenuItems as $id=>$menuItem) {
                            echo "<li><a";
                            $curDir = ($curDir == '/public_html') ? '/' : $curDir;
                            if($menuItem[1] == $curDir) {
                                echo " class='selected' ";
                            }
                             echo " href=".$menuItem[1]."><span>$menuItem[0]</span></a></li>";
                        } ?>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
        <div class="wrapper">
